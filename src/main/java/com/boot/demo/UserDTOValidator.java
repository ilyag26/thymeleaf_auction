package com.boot.demo;

import com.boot.demo.Users.BidDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import static org.springframework.util.ObjectUtils.isEmpty;

    @Component
    public class UserDTOValidator implements Validator {
        @Override
        public boolean supports(Class<?> clazz) {
            return BidDTO.class.equals(clazz);
        }

        @Override
        public void validate(Object target, Errors errors) {
            BidDTO user = (BidDTO) target;
            if (isEmpty(user.getName())) {
                errors.rejectValue("name", "400", "Login should not be empty!");
            }
            if (isEmpty(user.getBid())) {
                errors.rejectValue("bid", "400", "Password should not be empty!");
            }
        }

    }

