package com.boot.demo.PagesControllers;

import com.boot.demo.Services.UserService;
import com.boot.demo.Users.BidDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.sql.SQLException;
import java.util.List;

import static com.boot.demo.Configurations.Path.USER_LIST;
@Controller
public class ExampleController {

    @Autowired
    UserService userService;

    @RequestMapping(USER_LIST)
    public String getList(Model model) throws SQLException {
        List<BidDTO> all = userService.getAll();
        model.addAttribute("listExample", all);
        return "listExample";
    }

}
