package com.boot.demo.PagesControllers;

import com.boot.demo.Configurations.Path;
import com.boot.demo.Users.BidDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BidController {

    @RequestMapping(Path.BID)
    public String get(Model model) {
        model.addAttribute("user", new BidDTO());
        return "bid";
    }


}
