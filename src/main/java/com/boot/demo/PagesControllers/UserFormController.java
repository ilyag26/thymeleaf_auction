package com.boot.demo.PagesControllers;

import com.boot.demo.Configurations.Path;
import com.boot.demo.Services.UserService;
import com.boot.demo.UserDTOValidator;
import com.boot.demo.Users.BidDTO;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@AllArgsConstructor (onConstructor = @__(@Autowired))
public class UserFormController {

    private final UserDTOValidator userDTOValidator;

    private final UserService userService;

    @InitBinder
    public void userDTOBinder(WebDataBinder webDataBinder) {
        webDataBinder.addValidators(userDTOValidator);
    }

    @RequestMapping(value = Path.BID, method = POST)
    public String save(@Validated BidDTO user, BindingResult bindingResult, Model model) throws SQLException {
        if (bindingResult.hasErrors()) {
            Map<String, String> errors = new HashMap<>();
            bindingResult.getAllErrors().forEach(error -> errors.put(((FieldError)error).getField(), error.getDefaultMessage()));
            model.addAttribute("errors", errors);
            model.addAttribute("user", user);
            return "bid";
        }
        userService.save(user);
        return "listExample";
    }
}
