package com.boot.demo.Users;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
@Accessors(chain = true)
public class BidDTO {

    @NotEmpty
    private String name;

    @NotEmpty
    private String bid;

}
