package com.boot.demo.Repositories;


import com.boot.demo.Entity.BidEntity;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class BidRepository {


    private final javax.sql.DataSource dataSource;

    public List<BidEntity> getAll() throws SQLException {
        List<BidEntity> userEntities = Lists.newArrayList();
        PreparedStatement preparedStatement = dataSource.getConnection().prepareStatement("SELECT * FROM user");
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            
            String name = resultSet.getString("id");
            String bid = resultSet.getString("bid");
            userEntities.add(new BidEntity().setName(name).setBid(bid));
            
        }
        return userEntities;
    }

    public void save(String name, String bid) throws SQLException {

        PreparedStatement preparedStatement =
                dataSource.getConnection().prepareStatement("INSERT INTO user (name, bid) VALUES(? ,?)");

        preparedStatement.setString(1, name);
        preparedStatement.setString(2, bid);

        preparedStatement.execute();
    }

}
