package com.boot.demo.Entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class BidEntity {

    private int id;
    private String name;
    private String bid;

}
