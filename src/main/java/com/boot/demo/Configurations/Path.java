package com.boot.demo.Configurations;

public class Path {

    public static final String HOME = "/";

    public static final String BID = "/bid";

    public static final String SELLER = "/seller";

    public static final String JS = "/javas";

    public static final String USER_LIST = BID + "/list";
}
