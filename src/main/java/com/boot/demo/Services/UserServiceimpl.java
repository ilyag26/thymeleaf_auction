package com.boot.demo.Services;

import com.boot.demo.Repositories.BidRepository;
import com.boot.demo.Users.BidDTO;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

    @Service
    @AllArgsConstructor(onConstructor = @__(@Autowired))
    public class UserServiceimpl implements UserService {
        private final BidRepository userRepository;

        public void save(BidDTO user) throws SQLException {
            userRepository.save(user.getName(), user.getBid());
        }

        @Override
        public List<BidDTO> getAll() throws SQLException {
            return userRepository.getAll().stream()
                    .map(user -> new BidDTO().setName(user.getName()).setBid(user.getBid()))
                    .collect(Collectors.toList());
        }
    }

