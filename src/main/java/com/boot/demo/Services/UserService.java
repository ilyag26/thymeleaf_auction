package com.boot.demo.Services;

import com.boot.demo.Users.BidDTO;

import java.sql.SQLException;
import java.util.List;

public interface UserService {
    void save(BidDTO bidDTO) throws SQLException;
    List<BidDTO> getAll() throws SQLException;
}

